from pathlib import Path
import unittest
from selenium import webdriver


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.project_folder = Path("test/test.html").absolute().as_uri()
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_open_page_and_find_title(self):
        # Edith has heard about a cool new template system. She
        # Edith opens the webpage and finds the word ZML in the title
        # to check out its homepage
        self.browser.get(self.project_folder)

        # She notices the page title and header mention to-do lists
        self.assertIn('ZML', self.browser.title)
        self.fail('Finish the test!')


if __name__ == '__main__':
    unittest.main(warnings='ignore')
